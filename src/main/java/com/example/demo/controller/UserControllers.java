package com.example.demo.controller;

import com.example.demo.model.UserModel;
import com.example.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
//@RequiredArgsConstructor
@ComponentScan
public class UserControllers {

    private final UserRepository userRepository;

    @Autowired
    public UserControllers(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @GetMapping("/user/{customerId}")
    public UserModel user(@PathVariable Integer customerId) {
        UserModel user1 = new UserModel();
        user1.setName("Ngoc tan");
        UserModel user2 = new UserModel(2, "Ngoc Tan 2");

        return user1;
    }

//    @GetMapping("/user")
//    public UserModel user() {
//        UserModel user1 = new UserModel();
//        user1.setName("Ngoc tan");
//        UserModel user2 = new UserModel(2, "Ngoc Tan 2");
//
//        return user1;
//    }


}
