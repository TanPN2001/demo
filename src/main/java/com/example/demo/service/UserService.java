package com.example.demo.service;

import com.example.demo.model.UserModel;

public interface UserService {

    public UserModel addUser(UserModel user);
}
